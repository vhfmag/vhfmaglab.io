---
title: The magical, mathematical fictions of Jorge Luis Borges
links:
    - link: https://www.youtube.com/watch?v=mJeLGd3JV2I
      types: [bookmark-of]
syndicationLinks:
    - https://brid.gy/publish/twitter
twitterContent: sobre Borges e suas explorações do infinito ❤ https://www.youtube.com/watch?v=mJeLGd3JV2I
---

<div class="embed-container"><iframe src="https://www.youtube.com/embed/mJeLGd3JV2I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

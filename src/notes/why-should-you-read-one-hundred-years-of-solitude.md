---
title: Why should you read "One Hundred Years of Solitude"?
links:
    - link: https://www.youtube.com/watch?v=B2zhLYz4pYo
      types: [bookmark-of]
syndicationLinks:
    - https://brid.gy/publish/twitter
twitterContent: por que ler 'Cem Anos de Solidão'? (esse e Amor nos Tempos do Cólera, de Gabriel García Marquez, são LINDOS) https://www.youtube.com/watch?v=B2zhLYz4pYo
---

<div class="embed-container"><iframe src="https://www.youtube.com/embed/B2zhLYz4pYo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
